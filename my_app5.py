import pandas_datareader.data as web
import datetime
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import pandas as pd
from flask import Flask, render_template, redirect, url_for, request
import plotly
import random
from threading import Event
import plotly.graph_objs as go
from collections import deque

X = deque(maxlen=20)
Y = deque(maxlen=20)
X.append(1)
Y.append(1)

app = dash.Dash(__name__)

app.layout = html.Div(children=
    html.Div([
        dcc.Graph(id='live-graph', animate=True),
        dcc.Interval(
            id='graph-update',
            interval=1000
        )
    ])
)


@app.callback(Output('live-graph', 'figure'),
    events=[Event('graph-update', 'interval')])
def update_graph():
    global X
    global Y
    X.append(X[-1]+1)
    Y.append(Y[-1] + (Y[-1]*random.uniform(-0.1, 0.1)))
    data = go.Scatter(
        list(X),
        list(Y),
        name='graph',
        mode='lines+markers'
    )
    return {
        'data':[data],
        'layout':go.Layout(
            xaxis=dict(range=[min((X), max(X))]),
            yaxis=dict(range=[min((Y), max(Y))])
        )}



'''dcc.Graph(id='ex1', figure={
        'data':[
            {'x':[1,2,5,7,9], 'y':[6,2,4,7,1], 'type':'line', 'name':'boats'},
            {'x':[1,3,6,8,10], 'y':[1,2,4,7,1], 'type':'bar', 'name':'cars'},
        ],
        'callbackwithstate':{'title':'Basic example'}
    })'''

if __name__ == '__main__':
    app.run_server(debug=True)
