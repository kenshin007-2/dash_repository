import dash
import dash_core_components as dcc
import dash_html_components as html

app = dash.Dash()

app.layout = html.Div(children=[
    html.H1('Dash tutorialsss'),
    html.H2('Dash tutorialsss 222'),
    dcc.Graph(id='ex1', figure={
        'data':[
            {'x':[1,2,5,7,9], 'y':[6,2,4,7,1], 'type':'line', 'name':'boats'},
            {'x':[1,3,6,8,10], 'y':[1,2,4,7,1], 'type':'bar', 'name':'cars'},
        ],
        'callbackwithstate':{'title':'Basic example'}
    })
])


if __name__ == '__main__':
    app.run_server(debug=True)
