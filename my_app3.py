import pandas_datareader.data as web
import datetime
import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
from flask import Flask, render_template, redirect, url_for, request

start = datetime.datetime(2015, 1, 1)
end = datetime.datetime(2018, 2, 8)
#TSLA2 = 'https://github.com/curran/data/blob/gh-pages/all/d3Examples_AAPL.csv'
#google2 = 'http://finance.google.com/finance/historical'
#stock = url_for('static', filename='datafile')#'TSLA' 'google'
stock = 'iex'
#pd.read_csv('Dashlayout/static/datafile.csv', sep=',')#'TSLA'
df = web.DataReader('GOOGL', stock, start, end)
#df2 = web.DataReader('Dashlayout/static/datafile.csv', , )
#print(df.head())
#print(stock['A0'])
#print()
#print(df.columns)

app = dash.Dash()
app.layout = html.Div(children=[
    html.H1(children='Dash'),

    html.Div(children='''
        Dash: New app
    '''),

    dcc.Graph(
        id='example-graph',
        figure={
            'data': [
                {'x': df.index, 'y': df.close, 'type': 'line', 'name': 'SF'},
            ],
            'layout': {
                'title': 'Dash Data Visualization'
            }
        }
    )
])

if __name__ == '__main__':
    app.run_server(debug=True)

