import pandas_datareader.data as web
import datetime
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import pandas as pd
from flask import Flask, render_template, redirect, url_for, request

# TSLA2 = 'https://github.com/curran/data/blob/gh-pages/all/d3Examples_AAPL.csv'
# google2 = 'http://finance.google.com/finance/historical'
# stock = url_for('static', filename='datafile')#'TSLA' 'google'
# pd.read_csv('Dashlayout/static/datafile.csv', sep=',')#'TSLA'
# df2 = web.DataReader('Dashlayout/static/datafile.csv', , )
# print(df.head())
# print(stock['A0'])
# print()
# print(df.columns)

app = dash.Dash()

app.layout = html.Div(children=[
    html.H1(children='Dash'),

    html.Div(children='''
        Dash: New app
    '''),
    dcc.Input(id='input', value='', type='text'),
    html.Output(id='output-graph')
])

@app.callback(
    Output(component_id='output-graph', component_property='children'),
    [
        Input(component_id='input', component_property='value'),
     ]
)
def update_graph(input_data):
    start = datetime.datetime(2015, 1, 1)
    end = datetime.datetime.now()#(2017, 2, 8)
    stock = 'iex'
    #df = web.DataReader('GOOGL', stock, start, end)"AAPL", "google"
    df = web.DataReader('GOOGL', "iex", str(input_data), end)
    #print(df.head())
    #df = web.DataReader('F', 'iex', start, end)
    #print(df)

    return dcc.Graph(
        id='example-graph',
        figure={
            'data': [
                {'x': df.index, 'y': df.close, 'type': 'line', 'name': 'SF'},
            ],
            'layout': {
                'title': input_data
            }
        }
    )



if __name__ == '__main__':
    app.run_server(debug=True)

